#!/bin/bash

source $HOME/.profile

exec > $HOME/.local/log/startup.log
exec 2>&1

xrdb -load ~/.config/Xresources
xsetroot -cursor_name left_ptr

export SSH_AUTH_SOCK=${XDG_RUNTIME_DIR}/ssh-agent.socket

systemctl --user import-environment PATH DISPLAY XAUTHORITY SSH_AUTH_SOCK XDG_SESSION_ID

# Start all daemons
systemctl --user reload-or-restart xinitrc.target

autorandr -c -f
