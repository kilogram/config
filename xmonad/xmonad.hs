import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.StackTile
import XMonad.Layout.NoBorders
import XMonad.Layout.ShowWName
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run(spawnPipe)
import qualified XMonad.StackSet as W

import qualified Data.Map as M
import Data.Monoid
import System.Exit


scratchpads =
    [ NS "chrome" "google-chrome --new-window" (appName =? "chrome-float") (customFloating $ W.RationalRect 0.1 0.1 0.9 0.9)
    , NS "tmux" "tmuxstart" (appName =? "scratchpad") (customFloating $ W.RationalRect 0.1 0.1 0.9 0.9)
    ]

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@(XConfig {XMonad.modMask = modMask'}) = M.fromList $
    -- launching and killing programs
    [ ((modMask'              , xK_space ), spawn $ XMonad.terminal conf) -- %! Launch terminal
    , ((modMask',               xK_z     ), spawn "dmenu_run") -- %! Launch dmenu
    , ((modMask',            xK_BackSpace), kill) -- %! Close the focused window

    , ((modMask',               xK_backslash), sendMessage NextLayout) -- %! Rotate through the available layout algorithms
    , ((modMask' .|. shiftMask, xK_backslash), setLayout $ XMonad.layoutHook conf) -- %!  Reset the layouts on the current workspace to default

    , ((modMask',               xK_n     ), refresh) -- %! Resize viewed windows to the correct size

    -- , ((modMask',               xK_Tab   ), nextScreen)
    -- , ((modMask' .|. shiftMask, xK_Tab   ), shiftNextScreen >> nextScreen)

    -- move focus up or down the window stack
    , ((modMask',               xK_j     ), windows W.focusDown) -- %! Move focus to the next window
    , ((modMask',               xK_k     ), windows W.focusUp  ) -- %! Move focus to the previous window
    , ((modMask',               xK_m     ), windows W.focusMaster  ) -- %! Move focus to the master window

    -- modifying the window order
    , ((modMask' .|. shiftMask, xK_space ), windows W.swapMaster) -- %! Swap the focused window and the master window
    , ((modMask' .|. shiftMask, xK_j     ), windows W.swapDown  ) -- %! Swap the focused window with the next window
    , ((modMask' .|. shiftMask, xK_k     ), windows W.swapUp    ) -- %! Swap the focused window with the previous window

    -- resizing the master/slave ratio
    , ((modMask',               xK_h     ), sendMessage Shrink) -- %! Shrink the master area
    , ((modMask',               xK_l     ), sendMessage Expand) -- %! Expand the master area

    -- floating layer support
    , ((modMask',               xK_t     ), withFocused $ windows . W.sink) -- %! Push window back into tiling

    -- increase or decrease number of windows in the master area
    , ((modMask'              , xK_comma ), sendMessage (IncMasterN 1)) -- %! Increment the number of windows in the master area
    , ((modMask'              , xK_period), sendMessage (IncMasterN (-1))) -- %! Deincrement the number of windows in the master area

    -- quit, or restart
    , ((modMask' .|. shiftMask, xK_q     ), io (exitWith ExitSuccess)) -- %! Quit xmonad
    , ((modMask' .|. shiftMask, xK_r     ), spawn "if type xmonad; then xmonad --recompile && xmonad --restart; else xmessage xmonad not in \\$PATH: \"$PATH\"; fi") -- %! Restart xmonad

    , ((modMask'              , xK_w     ), namedScratchpadAction scratchpads "tmux")
    , ((modMask'              , xK_e     ), namedScratchpadAction scratchpads "chrome")
    ]
    ++
    -- mod-[1..9] %! Switch to workspace N
    -- mod-shift-[1..9] %! Move client to workspace N
    [((m .|. modMask', k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    -- mod-{w,e,r} %! Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r} %! Move client to screen 1, 2, or 3
    [((m .|. modMask', key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_a, xK_s, xK_d] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


manageWindows :: Query (Data.Monoid.Endo WindowSet)
manageWindows = composeAll
                    [ className =? "Pavucontrol" --> (doRectFloat $ W.RationalRect 0.3 0.3 0.4 0.4)
                    , className =? "Steam" --> doFloat
                    , title =? "Steam - Update News" --> doFloat
                    , isFullscreen --> doFloat
                    , className =? "conky" --> doIgnore
                    ]


main :: IO ()
main = do
    -- xmproc <- spawnPipe "/usr/bin/xmobar /home/kilogram/.xmonad/xmobar.conf"
    xmonad $ ewmh defaultConfig
        { normalBorderColor = "#333333"
        , focusedBorderColor = "#daa520"
        , manageHook = namedScratchpadManageHook scratchpads
                        <+> manageWindows
                        <+> manageDocks
                        <+> manageHook defaultConfig
        , handleEventHook = XMonad.Hooks.EwmhDesktops.fullscreenEventHook
                            <+> handleEventHook defaultConfig
        , layoutHook = smartBorders
          $ avoidStruts
          $ Tall 1 (3/100) (1/2)
          ||| Mirror (Tall 1 (3/100) (1/2))
          ||| StackTile 1 (3/100) (3/4)
          ||| (noBorders Full)
        , startupHook = (spawn "~/.config/session/startup.sh") <+> startupHook defaultConfig
        -- , logHook = dynamicLogWithPP xmobarPP { ppOutput = hPutStrLn xmproc
        --                                       , ppTitle = xmobarColor "green" "" . shorten 50
        --                                       }
        , modMask = mod4Mask
        , keys = myKeys
        , terminal = "urxvtc"
        }
